System.config({

    paths: {
        'systemjs': '../../node_modules/systemjs/dist/system.src.js',
        'typescript': '../../node_modules/typescript/lib/typescript.js',
        'phantomjs-polyfill': '../../node_modules/phantomjs-polyfill/bind-polyfill.js',
        'system-polyfills'  : '../../node_modules/systemjs/dist/system-polyfills.js'
    },

    transpiler: 'typescript'
});
