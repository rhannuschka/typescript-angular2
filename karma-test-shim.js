/**
 * @TODO refactor karma shim file
 */

Error.stackTraceLimit = Infinity;

System.config({
  'meta': {
    'polyfill.js': {
      format: 'global'
    }
  },
  packages: {
    '/base/src': {
       map: Object.keys(window.__karma__.files)
            .filter(onlyAppFiles)
            .reduce(createPathRecords, {})
    }
  }
});

// disable __karma__.start
// and run karma first if all files are loaded 
(function(karmaStart) {
    __karma__.start = function() {};
    Promise.all(resolveTestFiles())
      .then(function() {
          karmaStart();
      },
      function(error) { 
        __karma__.error(error.stack || error); 
      });
}(__karma__.start));

function createPathRecords(pathsMapping, appPath) {
  var pathParts = appPath.split('/');
  var moduleName = './' + pathParts.slice(-1).join('/');
  moduleName = moduleName.replace(/\.js$/, '');
  pathsMapping[moduleName] = appPath;
  return pathsMapping;
}

function onlyAppFiles(filePath) {
  return /\/base\/src\/(?!.*\.spec\.js$).*\.js$/.test(filePath);
}

function onlySpecFiles(path) {
  return /\.spec\.js$/.test(path);
}

function resolveTestFiles() {

  System.import('polyfill.js');
  return Object.keys(window.__karma__.files)  // All files served by Karma.
    .filter(onlySpecFiles)
    .map(function(moduleName) {
        console.log ( moduleName );
      // loads all spec files via their global module names (e.g.
      // 'base/dist/vg-player/vg-player.spec')
      return System.import(moduleName);
    });
}
