// Karma configuration
// Generated on Sat Nov 19 2016 19:19:41 GMT+0100 (CET)
module.exports = function(config) {
  config.set({

    basePath: './',

    // base path that will be used to resolve all patterns (eg. files, exclude)

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ["systemjs", "mocha", "chai", "sinon"],

    // list of files / patterns to load in the browser
    files: [
       {pattern: 'karma-test-shim.js', included: true, watched: true},
       {pattern: 'polyfill.js', included: false},
       {pattern: 'src/**/*.ts', included: false, watched: false},
       {pattern: 'tests/*.spec.ts', included: false, watched: false},
       {pattern: 'tests/mockup/*.ts', included: false, watched: false}
    ],

    // list of files to exclude
    exclude: [
      '**/*.swp'
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        "src/**/*.ts": ["typescript", "babel"],
        "tests/**/*.ts": ["typescript", "babel"]
    },

    typescriptPreprocessor: {
      typings: ['typings/tsd.d.ts']
    },

    babelPreprocessor: {
        "options": {
            presets: ['es2015'],
            sourceMap: 'inline'
        },
        "filename": function (file) {
            /**
             * babel translator preprocessor take as default the originalPath
             * in this case this would be a .ts file and not a js file
             *
             * so return the file path the typescript preprocessor returns
             */
            return file.path;
        },
        "sourceFileName": function (file) {
            return file.originalPath;
        }
    },

    plugins: [
        "karma-phantomjs-launcher",
        "karma-systemjs",
        "karma-chai",
        "karma-sinon",
        "karma-mocha",
        "karma-mocha-reporter",
        "karma-systemjs",
        "karma-typescript-preprocessor",
        "karma-babel-preprocessor"
    ],

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['mocha'],

    systemjs: {
       configFile: 'system.conf.js'
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
};
