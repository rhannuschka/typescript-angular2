/**
 * requirejs build file
 */
require.config({

    paths: {

        'text': '../vendor/text/text',

        'jquery': 'empty:',

        'jquery.ui': 'empty:',
        
        'vendor': '../vendor',

        'guestbook': 'app/guestbook',

        'member': 'app/member',

        'gmap': 'app/gmap',

        'requirejs-plugins': '../vendor/requirejs-plugins'
    },

    removeCombined: true,
    
    map: {
        '*' : {
            'jquery.pagination': 'lib/jquery.plugins/jquery.simplePagination',
            
            'handlebars': 'vendor/handlebars/handlebars.min',

            'viewhelpers': 'viewhelper/index',

            'async': 'requirejs-plugins/src/async'
        }
    },

    shim: {
        'lib/jquery.plugins/jquery.simplePagination': '[jquery]'    
    },
    
    optimize: 'uglify2',
     
    preserveLicenseComments: false,
     
    generateSourceMaps: true,
     
    modules: [
        {
            name: 'boot',
            include: [
				'config/common',
                'app/main',
                'service/TemplateService',
                'service/WidgetService',
                // libaries
                'lib/Pagination',
                //include all custom viewhelpers,
                'viewhelpers',
                // requirejs plugings
                'async',
                // views 
                'view/PaginationPanel',
                'view/Form',
                // templates
                'template/container',
                'template/pagination',
                'template/panel',
                'template/header',
                'template/paginate',
                'template/form/index',
                'template/form/error'
            ]
        },
        {
            name: 'guestbook/Guestbook',
            include: [
                'guestbook/Form',
                'guestbook/Page',
                'guestbook/template/page',
                'guestbook/template/form',
                'guestbook/template/index'
            ],
            exclude: [
                'lib/AppCache',
                'view/Form',
                'view/PaginationPanel'
            ]
        },
        {
            name: 'member/Member',
            include: [
                'member/template/index'
            ],
            exclude: [
                'lib/AppCache',
                'view/PaginationPanel'
            ]
        },
        {
            name: 'gmap/Approach',
            include: [
                'gmap/template/container'
            ],
            exclude: [
                'lib/AppCache',
                'view/Panel'
            ]
        }
    ]
})
