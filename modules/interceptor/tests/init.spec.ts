/// <reference path="../typings/globals/mocha/index.d.ts"/>
/// <reference path="../typings/modules/chai/index.d.ts"/>

import {Interceptor} from '../src/interceptor';
import {ObserverMock} from './mockup/observer.js';
import {Observable, Observer} from '../src/observable';

describe('Interceptor:Class', () => {

  describe('Interceptor:Base', () => {

    it('should be a singleton', () => {
        expect(() => { new Interceptor(); }).to.throw(Error);
    });

    it('sould be get an Instance', () => {
        let interceptor = Interceptor.getInstance();
        expect( interceptor instanceof Interceptor ).to.be.true;
    });

    it('sould have subscribe method', () => {
        let interceptor = Interceptor.getInstance();
        expect ( typeof interceptor.subscribe ).to.be.equal("function");
    });

    it('sould have unsubscribe method', () => {
        let interceptor = Interceptor.getInstance();
        expect ( typeof interceptor.unSubscribe ).to.be.equal("function");
    });

    it('sould have notify method', () => {
        let interceptor = Interceptor.getInstance();
        expect ( typeof interceptor.notifyObserver ).to.be.equal("function");
    });
  });

  describe('Interceptor:Subscribe', () => {

    let interceptor;
    let observerMock;
    
    before(() => {
        interceptor  = Interceptor.getInstance();
    });

    beforeEach(() => {
        observerMock = new ObserverMock();
    });

    it('should possible to subscribe an observer', () => {
        interceptor.subscribe(ObserverMock, '*');
    });
  });
});
