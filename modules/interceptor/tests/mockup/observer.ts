import {Observer} from '../../src/observable';

class ObserverMock implements Observer
{
    public notify () 
    {
        console.log ("message");
    }
}

export {ObserverMock};
