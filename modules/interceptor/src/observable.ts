interface Observable {

    subscribe:(observer: Observer, url: string) => void;

    unSubscribe: (observer: Observer, url: string) => void;

    notifyObserver: (...args: any[]) => void;
}

interface Observer 
{
    notify: () => void;
}

export {Observable, Observer};
