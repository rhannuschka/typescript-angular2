import {Observable, Observer} from './observable';

interface XHRData
{
  method: string,
  url: string
}

class Interceptor implements Observable
{
  private static instance:Interceptor = new Interceptor();

  /**
   * @type WeakMap
   * @private
   */
  private observers;

  /**
   * @type Map
   * @private
   */
  private requests;

  /**
   * constructor
   * @private
   */
  constructor () {

    if (Interceptor.instance) {
      throw Error("Error: use Interceptor.getInstance()");
    }

    this.requests = new WeakMap();
    this.observers = new Map();
    this.observers.set('*', new WeakSet());

    this.decorateXMLHttpOpen(XMLHttpRequest.prototype.open);
    this.decorateXMLHttpSend(XMLHttpRequest.prototype.send);
  }

  /**
   * decorate xml http request open method
   */
  private decorateXMLHttpOpen ( open ): void
  {
    let self = this;

    XMLHttpRequest.prototype.open = function (method: string, url: string) 
    {
      let xhr = this;
      let requestData:XHRData = {
          method: method,
          url   : url
      };

      self.requests.add(xhr, requestData);

      xhr.addEventListener('readystatechange', () => {
         self.onXMLHttpChangeState(xhr) });

      open.apply(xhr, arguments);
    }
  }

  /**
   * decorate xml http request send method
   */
  private decorateXMLHttpSend ( send ): void
  {
    let self = this;

    XMLHttpRequest.prototype.send = function(data) {
      let requestData = self.requests.get(this);
      let xhr = this;

      self.notifyObserver(xhr, requestData, 'send');
      send.apply(xhr, arguments);
    };
  }

  /**
   * @param {XMLHttpRequest} xhr
   * @returns void
   */
  private onXMLHttpChangeState (xhr: XMLHttpRequest): void
  {
    if ( xhr.status !== XMLHttpRequest.DONE ) {
      return;
    }

    let data:XHRData = this.requests.get(xhr);

    if ( xhr.status >= 200 && xhr.status < 300 || xhr.status === 304 ) {
      this.notifyObserver(xhr, data, 'success');
    } else {
      this.notifyObserver(xhr, data, 'error');
    }

    this.notifyObserver(xhr, data, 'done');
    this.requests.delete(xhr);
  }

  /**
   * register observer
   *
   * @param {Observer} observer
   * @param {String} url
   * @public
   */
  public subscribe (observer: Observer, url: string = '*'): void
  {
    let urlObserverSet = this.observers.get(url);

    if ( urlObserverSet )
    {
      this.observers.set( url, new WeakSet() );
      urlObserverSet = this.observers.get(url);
    }
    urlObserverSet.add(observer);
  }

  /**
   * un subscribe from observer
   *
   * @param {Observer} observer
   * @param {String} url
   */
  public unSubscribe (observer: Observer, url: string = '*'): void
  {
    let observerSet; 

    if ( this.observers.has(url) )
    {
      observerSet = this.observers.get('url');
      observerSet.delete( observer );
    }
  }

  /**
   * notify all observers
   *
   * @param {XMLHttpRequest} xhr, the XHR request was send
   * @param {XHRData} data , data object
   * @param {String} status
   * @return void
   */
  public notifyObserver (xhr: XMLHttpRequest, data: XHRData, status: string): void
  {
    let urlObserverSet    = this.observers.get(data.url);
    let globalObserverSet = this.observers.get('*');

    for(let observer of [...globalObserverSet, ...urlObserverSet])
    {
      observer.notify(status, xhr, data);
    }
  }

  /**
   * returns a Interceptor Instance
   */
  public static getInstance():Interceptor {
    return Interceptor.instance;
  }
}

export {Interceptor};
